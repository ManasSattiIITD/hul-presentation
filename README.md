# hul-presentation

## ACT - 1

- Manas : Par Sahab lash sad rahi hai wahan. Kam se kam use kapde se toh dhak dete hain.  

- Dhananjay : Nahi! Jab tak ye mamla sulajh nahi jaata, aur vo lash hamare hathon mein na aa jaye, tab tak usko chuna bhi mat.  
              Agar ye lash vo Khanpur wale thanedar kii nikli, toh ispe kari sari mehnat bekar ho jayegi. Lash sadti hai toh 
              Sadne do, aur uspe kadi nazar rakho. Vese bhi ye sab tumhari galti hai.

- Manas : Meri sahab?

- Dhananjay : Haan, aur kya. Kaun tha vo aadmi? Use jane kaise diya?

- Manas : Sahab usne bola kii koi pull ke neeche pada hua hai. Mujhe kuch aur na soojha bas us pull ke neeche pade aadmi kii 
            jaanch karne ke liye nikal pada.

- Dhananjay : Mujhe kyu nahi bulaya tumne ? 

- Manas : Sahab mujhe laga kii hamara andruni mamla hai, ham dekh lenge, vo toh jab mein wahan pahoncha tab pata chala kii khanpur ka thanedar 
          wahan pehle se aa chuka tha. Tab meine aapko-

- Dhananjay : Haan haan mereko bulaya aur mujhe unko khaded ke bhagana pada. Mein toh kab se bol raha hoon, agar mujhe pehle hii bula liya hota, 
              toh vo aadmi marta hii nahi. 

- Manas : [muttering] Sahab gulam-sa hii hoga gaya tha, accha hai mar gaya. 

- Dhananjay : Kya badbada rahe ho. Tum logon se kuch kaam toh hota nahi hai, aur vaise bhi kaisa kam-akal tha kii khanpur kii sarhad pe khud 
              ko mehfooz samajh raha tha. Un logon kii taraf khada hua tha. Arey vo bahot kattar aur chalak log hai.
                - 
              Chalo lagta hai vo patrakar aa gaya. Kuch chota mota hota nahi hai aur ye log aan padte hain....

## ACT - 2

- Dhananjay : Aao patrakar sahab. Vaise bura mat manna par jis mamle ke baare mein aap puchne aye ho, 
              vo kaafi takniki hai aur aapki samajh se pare ho sakta hai. 

- Yash : Toh ye jo lash mili hai- 

- Dhananjay : Lash ka mamla nahi hai ye. Ye uske sar ka mamla hai. Sawal ye hai kii uska sir kis taraf tha. Mujhe uske chamdi aur maas se kya matlab. 
              Uska Sar, uska sar kis disha mein tha!

- Yash : Toh aapko nahi pata kii uska sar kis taraf tha?

- Dhananjay : Aisa kuch hai hii nahi jo hame na pata ho. Wo log bewakoof hain agar unko ye lagta hai kii wo baat ghuma ke hame hara denge. 
              Ham unki tarah kacche khiladi nahi hain. Wo log toh vaise bhi rishwat de de ke thanedar bane hain. Pichle 30 saalon se hamari rozi roti 
              is kaam pe chal rahi hai. Hamare saath chalaki nahi.

- Yash : Toh ye khabar toh aapko deri se pata chali hogi, warna khanpur ke thanedar apse pehle kaise hii pahonch sakte hain?

- Dhananjay : [smile] Ye toh hai. [normal] Ye sab us hawaldar kii galti hai, tum jante ho kya use?

- Yash : Haan, haan...

- Dhananjay : Ek aadmi ko na pakad saka wo. Mujhe bhi nahi bulaya, warna mujhe sab pata hota. Par meine mere saare hisaab sambhal liye hain.

- Yash : Kaun tha wo aadmi

- Dhananjay : Ye toh pata chal hii nahi sakta, sab us hawaldar kii wajah se. Ye log bheek mangke bhi guzara nahi kar paate, isliye hawaldaar ban jaate hain. 
              Kam-akal kahinke. Inko samsya kii nazukta bhi nahi samajh aati.

- Yash : Toh Samasya kya hai? 

- Dhananjay : Samasya kuch nahi hai. Ye sab ham pehle bhi dekh chuke hain, aur hamare kagzaat pure hain. Mein duniya ke samne dikha dunga kii vo vyavsayi mamlon mein kitna nakabil hai.

- Yash : Toh thanedar se kya dikkat hai?

- Dhananjay : Jaise hii mujhe khabar mili kii poshmarg ke chinar pr lash mili hai, mein turant rawana ho gaya. Hame thodi der lag gayi, kyuki hamara thana uss khanpur ke thane jaisa 
              chota nahi hai. Hamara ilaqa bahot bada hai. Jab ham wahan pahonche toh vo vahan pehle se tha, lash chinar pe padi hui thi, meine uski chalaki turant bhaamp lii. 

- Yash : Toh vo lash kiske ilaqe mein thi? 
            [Dhananjay Exit]

- Manas : Sahab ko kuch kaam hai, isliye unhone aapke sawalon ka jawab mujhe dene ko kaha hai..

- Yash : Toh vo lash kiske ilaqe mein thi?

- Manas : Vo lash kisi ilaqe mein nahi thi, balki sarhad par padi hui thi. 

- Yash : Toh us lash kii jaanch kaun karega? 

- Manas : Dono thanedaron ka maanna hai kii jaanch ka haq usko hoga jiske ilaqe mein lash milti hai.

- Yash : Par jaisa kii aapne bola, lash toh sarhad pe thi, toh ab vo kiski hogi?

- Manas : Sahi baat hai, par Dono taraf ka ye manna hai kii jis taraf lash ka sar hota hai, jaanch ka haq, us ilaqe ko milta hai......

- Yash : Toh aapke sahab ne kya kadam uthaya? 

- Manas : Sahab bahot gusse mein the kii Khanpur ka thanedar wahan tha, unhone laathi se uske logon ko peeta. Usse baat badhti hai, par sahab mante hain kii unke kaagzaat kaafi hain. 
          Toh mamla abhi thehra hua hai..

## ACT - 3 

- Yash : Par lash ka kya haal hai.. Wahan pade pade toh ye sad jayegi..

- Manas : Sahab bolte hain kii sadne do. Unke liye zaroori baat ye hai kii lash ka sar kaha tha...

[Silence]

- Yash : Aapka kya khyal hai iss baare mein? 

- Manas : Ab yaha itni kaha parva logon ki kisi ko. Aur ye naukri bhi esi hii kii sahab ke aadesh ki bina chal bhi nahi sakti.
          
- Yash : Toh uss aadmi ke baare mein kya pata hai aapko?

- Manas : Suna hai kii kaafi nek aur khushaal insaan tha. 
          Par pita ke guzarne ke baad, uska yahan wala aur khanpur wala bhai ek dusre se ladne lage.
          Jiski wajah se bechara pareshaan rehne laga. Par kudrat ka bhi mazak dekho mara toh bhi dono kii dehleez par. 
          Ab dekhte hain uss lash ke sath aur kya kya khel khele jayenge.......

[Stares for 5 sec]
[Play ends]